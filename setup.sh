#!/bin/bash
check_status() {
  if [[ $1 != 0 ]]; then
    echo
    echo $2
    exit $1
  fi
}

rm -rf roles/galaxy
check_status $? 'Error when removing galaxy roles'

ansible-galaxy install -p roles/galaxy/ -r roles/requirements.yml
check_status $? 'Error when fetching galaxy roles'

ansible-playbook -i assets/hosts site.yml -e username=$USER
check_status $? 'Error when running playbook'

sudo ln -sf /home/docker /var/lib/docker
check_status $? 'Error when creating docker link in home folder'

echo 'Ansible install finished!'
