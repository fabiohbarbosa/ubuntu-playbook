#!/bin/bash
project_name=ubuntu-playbook

sudo su -c "echo '$USER ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers"
sudo apt-get install curl git ansible python-apt -y
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs -y
git-lfs install
git clone https://gitlab.com/fabiohbarbosa/${project_name}.git
cd ${project_name} && bash setup.sh
