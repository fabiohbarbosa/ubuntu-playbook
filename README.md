# Ubuntu Playbook #

[![build status](https://gitlab.com/fabiohbarbosa/ubuntu-playbook/badges/master/build.svg)](https://gitlab.com/fabiohbarbosa/ubuntu-playbook/commits/master)

## Motivation

Automate my linux machine installation.

See packages in `site.yml`


## Requirements

* Git LFS
* Ubuntu 16.04 or Ubuntu 17.04
* Linux Mint Sonya 18.2, Linux Mint 18.1 and Linux Mint 18 Sarah
* Sudo privilegies

## Install

```sh
$ curl -s https://gitlab.com/fabiohbarbosa/ubuntu-playbook/raw/master/pre-setup.sh | bash
```

## Contribute

Add a new playbook role and run `./test.sh`.  
The `test.sh` script will start a docker container and run the `setup.sh`.
